package com.sixtyninefourtwenty.stuff.annotations

/**
 * Denotes that for the annotated element, there's an equivalent way of doing the same thing in
 * Java for negligible differences in code length and general effort, which will be mentioned in [reason].
 */
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY)
@MustBeDocumented
@Retention
internal annotation class NotSuitableForJava(val reason: String)
