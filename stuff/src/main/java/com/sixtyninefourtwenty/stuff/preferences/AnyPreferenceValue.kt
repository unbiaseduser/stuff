package com.sixtyninefourtwenty.stuff.preferences

/**
 * Internal interface to prevent code duplication. There's no point referring to this directly.
 */
sealed interface AnyPreferenceValue {
    val value: Any
}