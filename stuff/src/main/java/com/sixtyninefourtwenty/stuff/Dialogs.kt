@file:JvmName("Dialogs")

package com.sixtyninefourtwenty.stuff

import android.app.Dialog
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog

/**
 * Make the [Dialog] wider.
 *
 * This is only useful for dialogs created by [AlertDialog.Builder]s to allow them to take up as
 * much screen width as possible. If you're creating a custom dialog, this method will probably
 * be of no use to you.
 */
fun Dialog.setToFullWidth() {
    /*
     * Use WRAP_CONTENT for height since setting that to MATCH_PARENT does nothing useful
     * AND breaks non-fullscreen content
     */
    window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
}
