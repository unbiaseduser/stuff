package com.sixtyninefourtwenty.stuff.interfaces

/**
 * Represents an object that has a [JsonSerializer].
 */
@Suppress("unused")
interface JsonSerializable<T> {
    val jsonSerializer: JsonSerializer<T>
}